public class Test
{
	public static void main(String args[])
	{
		Parser parser;
		parser = new Parser();
		boolean result;
		
		result = parser.program("Let foo = FALSE; LET bar = True; LET a = FALSE; QUERY (TRUE <=> Foo -> bar) -> a.");		

		System.out.println(result);
	}
}
