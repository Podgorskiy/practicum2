/*
 * Practicum 2
 * Stanislav Pidhorskyi
 * Ahmad Jamialahmadi
 * Dimitri Innocent
 */
import java.util.HashMap;

public class Parser 
{
	public boolean program(String sentence)
	{
		m_lexer = new Lexer(sentence);		
		m_lastToken = m_lexer.GetNextToken();
		m_variableTable = new VariableTable();
		
		while (AcceptAssignment())
		{}	
		
		Attribute attribute = new Attribute();
		
		ExpectQuery(attribute);
		
		ExpectToken(Lexer.TokenType.EOF);
		
		return attribute.toBool();
	}

	private boolean AcceptAssignment()
	{
		if (AcceptToken(Lexer.TokenType.LET))
		{
			String identifier = m_lastToken.GetName();
			ExpectToken(Lexer.TokenType.IDENTIFIER);
			ExpectToken(Lexer.TokenType.ASSIGN);
			
			Attribute attribute = new Attribute();
			
			if (!AcceptProposition(attribute))
			{
				throw new RuntimeException("Error, wrong assignment.");
			}
								
			ExpectToken(Lexer.TokenType.SEMICOLON);
			
			m_variableTable.SetVariableValue(identifier, attribute.toBool());
			
			return true;
		}
			
		return false;
	}
	
	private boolean ExpectQuery(Attribute attribute)
	{
		ExpectToken(Lexer.TokenType.QUERY);
		
		if (!AcceptProposition(attribute))
		{
			throw new RuntimeException("Error, wrong query.");
		}
		
		ExpectToken(Lexer.TokenType.DOT);
		
		return false;
	}
	
	private boolean AcceptProposition(Attribute attribute)
	{
		Attribute lhsAttribute = new Attribute();
		Attribute rhsAttribute = new Attribute();
		
		if (AcceptImplication(lhsAttribute))
		{
			while (AcceptToken(Lexer.TokenType.PROPOSITION))
			{
				if(!AcceptImplication(rhsAttribute))
				{
					throw new RuntimeException("Error, wrong proposition.");
				}
				lhsAttribute.Set(lhsAttribute.toBool() == rhsAttribute.toBool());
			}
			attribute.Set(lhsAttribute.toBool());
			return true;
		}
		return false;
	}
			
	private boolean AcceptImplication(Attribute attribute)
	{
		Attribute lhsAttribute = new Attribute();
		Attribute rhsAttribute = new Attribute();
		
		if (AcceptDisjunction(lhsAttribute))
		{
			if (AcceptToken(Lexer.TokenType.IMPLICATION))
			{
				if(!AcceptImplication(rhsAttribute))
				{
					throw new RuntimeException("Error, wrong impication.");
				}
				// Due to recursive call off AcceptImplication, 
				// this expression will be evaluated from right to left,
				boolean result = !(lhsAttribute.toBool() && !rhsAttribute.toBool());
				attribute.Set(result);
			}
			else
			{
				attribute.Set(lhsAttribute.toBool());
			}
			return true;
		}
		else
		{
			return false;
		}
	}
	
	private boolean AcceptDisjunction(Attribute attribute)
	{
		Attribute lhsAttribute = new Attribute();
		Attribute rhsAttribute = new Attribute();
		
		if (AcceptConjunction(lhsAttribute))
		{
			while (AcceptToken(Lexer.TokenType.DISJUNCTION))
			{
				if(!AcceptConjunction(rhsAttribute))
				{
					throw new RuntimeException("Error, wrong disjunction.");
				}
				lhsAttribute.Set(lhsAttribute.toBool() || rhsAttribute.toBool());
			}
			attribute.Set(lhsAttribute.toBool());
			return true;
		}
		return false;
	}
	
	private boolean AcceptConjunction(Attribute attribute)
	{
		Attribute lhsAttribute = new Attribute();
		Attribute rhsAttribute = new Attribute();
		
		if (AcceptNegation(lhsAttribute))
		{
			while (AcceptToken(Lexer.TokenType.CONJUNCTION))
			{
				if(!AcceptNegation(rhsAttribute))
				{
					throw new RuntimeException("Error, wrong conjunction.");
				}
				lhsAttribute.Set(lhsAttribute.toBool() && rhsAttribute.toBool());
			}	
			attribute.Set(lhsAttribute.toBool());
			return true;
		}
		return false;
	}
	
	private boolean AcceptNegation(Attribute attribute)
	{
		if (AcceptToken(Lexer.TokenType.NEGATION))
		{
			Attribute attributeToNegate = new Attribute();
			if (AcceptExpression(attributeToNegate))
			{
				attribute.Set(!attributeToNegate.toBool());
				return true;
			}
			else
			{
				throw new RuntimeException("Error, wrong negation.");			
			}
		}
		else
		{
			if (AcceptExpression(attribute))
			{
				return true;
			}
			return false;
		}	
	}
	
	private boolean AcceptExpression(Attribute attribute)
	{
		if (AcceptToken(Lexer.TokenType.LPAR))
		{
			if (AcceptProposition(attribute))
			{
				ExpectToken(Lexer.TokenType.RPAR);
				return true;
			}
			else
			{
				throw new RuntimeException("Error, wrong proposition after left parenthesis.");	
			}
		}
		else
		{
			if (AcceptBoolean(attribute))
			{
				return true;
			}
			else
			{
				return false;
			}
		}
	}
	
	private boolean AcceptBoolean(Attribute attribute)
	{
		if (AcceptToken(Lexer.TokenType.TRUE))
		{
			attribute.Set(true);
			return true;
		}
		else if(AcceptToken(Lexer.TokenType.FALSE))
		{
			attribute.Set(false);
			return true;
		}
		else 
		{
			String identifier = m_lastToken.GetName();
			if(AcceptToken(Lexer.TokenType.IDENTIFIER))
			{
				if (!m_variableTable.IsVariableDefined(identifier))
				{
					throw new RuntimeException(
							String.format("Error, undeclarated identifier: %s",
										  identifier));
				}
				attribute.Set(m_variableTable.GetVariableValue(identifier));
				return true;
			}
		}
		return false;
	}

	private boolean AcceptToken(Lexer.TokenType token)
	{
		if (m_lastToken.GetType() == token)
		{
			m_lastToken = m_lexer.GetNextToken();
			return true;
		}
		else
		{
			return false;
		}
	}
	
	private void ExpectToken(Lexer.TokenType token)
	{
		boolean accepted = AcceptToken(token);
		if (!accepted)
		{
			throw new RuntimeException(
					String.format("Error, unexpected token. Expected \"%s\", but \"%s\" found",
								  token, m_lastToken));
		}
	}
	
	private class Attribute
	{
		boolean toBool()
		{
			return m_bool;
		}
		
		void Set(boolean value)
		{
			m_bool = value;
		}
		
		private boolean m_bool;
	}	
	
	private class VariableTable
	{
		VariableTable()
		{
			m_table = new HashMap<String, Boolean>();
		}
					
		public boolean IsVariableDefined(String name)
		{
			return m_table.containsKey(name.toLowerCase());
		}

		public boolean GetVariableValue(String name)
		{
			return m_table.get(name.toLowerCase());
		}
		
		public void SetVariableValue(String name, boolean value)
		{
			m_table.put(name.toLowerCase(), value);
		}
		
		private HashMap<String, Boolean> m_table;
	}

	private static class Lexer
	{
		public static enum TokenType 
		{
			LET("LET"), 
			ASSIGN("="),	
			SEMICOLON(";"),
			QUERY("QUERY"),
			DOT("."),
			PROPOSITION("<=>"),
			IMPLICATION("->"),
			DISJUNCTION("|"),
			CONJUNCTION("&"),
			NEGATION("~"),
			LPAR("("),
			RPAR(")"),
			IDENTIFIER("IDENTIFIER"),
			TRUE("TRUE"),
			FALSE("FALSE"),
			EOF("EOF");

			TokenType(String lexem)
			{
				m_lexem = lexem;
			}

			@Override
			public String toString()
			{
				return m_lexem;
			}

			public Character toCharacter()
			{
				return m_lexem.charAt(0);
			}

			private final String m_lexem;
		}

		public class Token
		{		
			public Token(TokenType token, String identifier)
			{
				m_token = token;
				m_identifierName = identifier;
			}

			public Token(TokenType token)
			{
				m_token = token;
				m_identifierName = "";
			}

			public TokenType GetType()
			{
				return m_token;
			}

			public String GetName()
			{
				return m_identifierName;
			}

			@Override
			public String toString()
			{
				if(m_identifierName.length() > 0)
				{
					return m_token.toString() + ": " + m_identifierName;				
				}
				else
				{
					return m_token.toString();	
				}
			}

			private final TokenType m_token;
			private final String m_identifierName;
		}

		public Lexer(String string)
		{
			m_string = string;
			m_position = -1;
			NextCharacter();
		}
				
		public Token GetNextToken()
		{
			while (Character.isWhitespace(m_lastCharacter))
			{
				NextCharacter();
			}
			
			if (AcceptChar(TokenType.LPAR.toCharacter()))
			{
				return new Token(TokenType.LPAR);
			}
			else if (AcceptChar(TokenType.RPAR.toCharacter()))
			{
				return new Token(TokenType.RPAR);
			}
			else if (AcceptChar(TokenType.ASSIGN.toCharacter()))
			{
				return new Token(TokenType.ASSIGN);
			}
			else if (AcceptChar(TokenType.DISJUNCTION.toCharacter()))
			{
				return new Token(TokenType.DISJUNCTION);
			}
			else if (AcceptChar(TokenType.CONJUNCTION.toCharacter()))
			{
				return new Token(TokenType.CONJUNCTION);
			}
			else if (AcceptChar(TokenType.NEGATION.toCharacter()))
			{
				return new Token(TokenType.NEGATION);
			}
			else if (AcceptChar('<'))
			{
				ExpectChar('=');
				ExpectChar('>');
				return new Token(TokenType.PROPOSITION);
			}
			else if (AcceptChar('-'))
			{
				ExpectChar('>');
				return new Token(TokenType.IMPLICATION);
			}
			else if (AcceptChar(TokenType.DOT.toCharacter()))
			{
				return new Token(TokenType.DOT);
			}
			else if (AcceptChar(TokenType.SEMICOLON.toCharacter()))
			{
				return new Token(TokenType.SEMICOLON);
			}
			else if (AcceptChar('\0'))
			{
				return new Token(TokenType.EOF);
			}
			
			String identifier;
			identifier = "";
			
			while (Character.isAlphabetic(m_lastCharacter))
			{
				identifier += m_lastCharacter;
				NextCharacter();
			}
			
			String identifierUC = identifier.toUpperCase();
			
			if (identifierUC.equals("LET"))
			{
				return new Token(TokenType.LET);
			}
			else if (identifierUC.equals("QUERY"))
			{
				return new Token(TokenType.QUERY);
			}
			else if (identifierUC.equals("TRUE"))
			{
				return new Token(TokenType.TRUE);
			}
			else if (identifierUC.equals("FALSE"))
			{
				return new Token(TokenType.FALSE);
			}
			else if (identifier.length() > 0)
			{
				return new Token(TokenType.IDENTIFIER, identifier);
			}
			
			throw new RuntimeException(
						String.format("Error, unknown symbol \"%c\" at position %d.",
									  m_lastCharacter, m_position));
		}

		private boolean AcceptChar(Character c)
		{
			if (m_lastCharacter.equals(c))
			{
				NextCharacter();
				return true;
			}
			else
			{
				return false;
			}
		}
		
		private void ExpectChar(Character c)
		{
			boolean accepted = AcceptChar(c);
			if (!accepted)
			{
				if ('\0' == m_lastCharacter)
				{
					throw new RuntimeException(
						String.format("Error, unexpected end of string."));
				}
				else
				{
					throw new RuntimeException(
						String.format("Error, unexpected symbol at position %d. Expected \"%c\", but \"%c\" found",
									  m_position, c, m_lastCharacter));
				}
			}
		}
		
		private void NextCharacter()
		{
			++m_position;
			if (m_position >= m_string.length())
			{
				m_lastCharacter = '\0';
			}
			else
			{
				m_lastCharacter = m_string.charAt(m_position);					
			}		
		}
		
		private final String m_string;
		private Integer m_position;
		private Character m_lastCharacter;
	}
	
	private Lexer.Token m_lastToken;
	private Lexer m_lexer;
	private VariableTable m_variableTable;
}
